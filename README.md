# MARVEL - Swift

## Dependencies

pod 'SwiftLint'
Utilizado o swiftLint para garantir o mínimo de qualidade de código no projeto.

pod 'MKProgress'
Utilizado para exibir o Loading nas request executadas no projeto. 

pod 'Alamofire'
Camada de rede utilizada no projeto

pod 'AlamofireNetworkActivityLogger'
Logs das requests pra ajudar no desenvolvimento do projeto.

pod 'Kingfisher'
Utilizadado para Download de imagens no projeto.

pod 'SwiftMessages'
Utilizado para alerta nas views do projeto.
