//
//  CharactersDetailViewController.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 25/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class CharactersDetailViewController: BaseViewController {
    
    @IBOutlet weak var titleComicsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var constraintImage: NSLayoutConstraint!
    var viewModel: CharactersDetailViewModel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: CharactersDetailViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        setupDismissLeftBarButtonItem()
        viewModel.delegate = self
        showLoading(isLoading: true)
        viewModel.getListComics()
        title = "Characters Detail"
        setupView()
    }
    
    private func setupView() {
        constraintImage.constant = UIScreen.main.bounds.width
        initCollectionView()
        downloadImages(imageURL: self.viewModel.getImgeURL(thumbnail: viewModel.resultSelected?.thumbnail))
        descriptionLabel.text = viewModel.resultSelected?.description
    }

    private func downloadImages(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: url) { status in
            if case .failure = status {
                self.imageView.image = UIImage(named: "image_not_available")
            }
        }
    }
    
    private func initCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 150, height: 300)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.collectionViewLayout = layout
        collectionView.register(CharactersDetailCollectionViewCell.self)
    }
}

extension CharactersDetailViewController: UICollectionViewDataSource, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.listComicsResult.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(of: CharactersDetailCollectionViewCell.self,
                                                  for: indexPath, configure: { cell in
            let item = self.viewModel.listComicsResult[indexPath.row]
            cell.titleLabel.text = item.title
            cell.downloadImages(imageURL: self.viewModel.getImgeURL(thumbnail: item.thumbnail))

        })
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (collectionView.contentOffset.x + collectionView.frame.size.width) > collectionView.contentSize.width
            && viewModel.isLastPagination {
            showLoading(isLoading: true)
            viewModel.incrementPagination()
            viewModel.getListComics()
        }
    }
}

extension CharactersDetailViewController: CharactersDetailViewModelDelegate {
    func getListComicsFailure(error: GenericError) {
        alertError(error: error)
    }
    
    func showLoading(_ isLoading: Bool) {
        titleComicsLabel.text = viewModel.listComicsResult.count > 0 ? "Comics" : ""
        collectionView.reloadData()
        showLoading(isLoading: isLoading)
    }

}
