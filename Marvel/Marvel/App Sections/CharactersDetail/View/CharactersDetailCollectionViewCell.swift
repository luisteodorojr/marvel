//
//  CharactersDetailCollectionViewCell.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 25/03/21.
//

import UIKit

class CharactersDetailCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func downloadImages(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        imageView.kf.indicatorType = .activity
        imageView.kf.setImage(with: url) { status in
            if case .failure = status {
                self.imageView.image = UIImage(named: "image_not_available")
            }
        }
    }
}
