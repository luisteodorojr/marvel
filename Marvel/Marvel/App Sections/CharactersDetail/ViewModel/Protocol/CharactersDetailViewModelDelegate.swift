//
//  CharactersDetailViewModelDelegate.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 25/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation

protocol CharactersDetailViewModelDelegate: AnyObject {
    func showLoading(_ isLoading: Bool)
    func getListComicsFailure(error: GenericError)

}
