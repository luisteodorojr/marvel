//
//  CharactersDetailViewModel.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 25/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class CharactersDetailViewModel {
    
    var resultSelected: Results?
    var comicsResult: ComicsResult?
    var listComicsResult: [ComicsResults] = [ComicsResults]()
    weak var delegate: CharactersDetailViewModelDelegate?
    var limit = 20
    var offset = 0
    var pagination = 1
    var isLastPagination: Bool {
        return comicsResult?.data?.total ?? 0 >= offset
    }
    
    func getListComics() {
        let id = "\(resultSelected?.id ?? 0)"
        CharactersDetailApi().getListComics(parameters: getRequestConfig(), id: id, completion: { [weak self] result, _  in
            switch result {
            case .success(let aPIResult):
                self?.comicsResult = aPIResult
                self?.listComicsResult.append(contentsOf: aPIResult.data?.results ?? [ComicsResults]())
            case .failure(let error):
                self?.delegate?.getListComicsFailure(error: error)
            }
            self?.delegate?.showLoading(false)
        })
    }
    
    func getRequestConfig() -> RequestConfig {
        let ts = Date().timeIntervalSinceReferenceDate.description
        let publicAPIKey = MarvelAPIConfig.shared.publicAPIKey
        let hash = Crypto().MD5(data: "\(ts)\(MarvelAPIConfig.shared.privateAPIKey)\(publicAPIKey)")
        
        var requestModel = RequestConfig()
        requestModel.limit = "\(limit)"
        requestModel.offset = "\(offset)"
        requestModel.apikey = publicAPIKey
        requestModel.hash = hash
        requestModel.ts = ts
        return requestModel
    }
    
    func getImgeURL(thumbnail: Thumbnail?) -> String {
        return "\(thumbnail?.path ?? "").\(thumbnail?.extensionImage ?? "")"
    }
    
    func incrementPagination() {
        pagination += 1
        offset = pagination * limit
    }
}
