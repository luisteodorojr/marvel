//
//  CharactersDetailModel.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 25/03/21.
//

import UIKit

struct ComicsResult: Codable {
    var data: APIComicsData?
}

struct APIComicsData: Codable {
    var results: [ComicsResults]?
    var total: Int?
}

struct ComicsResults: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var thumbnail: Thumbnail?
}
