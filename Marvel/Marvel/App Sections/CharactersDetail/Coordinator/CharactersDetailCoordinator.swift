//
//  CharactersDetailCoordinator.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 25/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class CharactersDetailCoordinator {
    
    var viewModel: CharactersDetailViewModel!
    var viewController: CharactersDetailViewController!
    var navigation: UINavigationController?
    var resultSelected: Results?

    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func start() {
        viewModel = CharactersDetailViewModel()
        viewModel.resultSelected = resultSelected
        viewController = CharactersDetailViewController(viewModel: viewModel)
        navigation?.pushViewController(viewController, animated: true)
    }
    
    func stop() {
        viewModel = nil
        viewController = nil
        navigation = nil
    }
}
