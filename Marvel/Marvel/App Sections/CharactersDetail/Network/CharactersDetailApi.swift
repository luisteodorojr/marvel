//
//  CharactersDetailApi.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 25/03/21.
//

import UIKit

class CharactersDetailApi: Requester {
    func getListComics(parameters: RequestConfig, id: String,
                       completion: @escaping (Result<ComicsResult, GenericError>, URLResponse?) -> Void) {
        let request =  EndpointRouter.getListComics(parameters: parameters, id: id)
        dataTask(using: request, completion: completion)
    }
}
