//
//  HomeCoordinator.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class HomeCoordinator {
    
    var window: UIWindow
    var navigation: UINavigationController?
    var viewModel: HomeViewModel!
    var viewController: HomeViewController?
    var charactersDetailCoordinator: CharactersDetailCoordinator?

    required init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        viewModel = HomeViewModel()
        viewModel.coordinatorDelegate = self
        viewController = HomeViewController(viewModel: viewModel)
        viewController?.hidesBottomBarWhenPushed  = true
        navigation = UINavigationController(rootViewController: viewController!)
        navigation?.navigationBar.barTintColor =  UIColor(hex6: 0xED1D23)
        navigation?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        window.rootViewController = navigation
    }
    
    func stop() {
        navigation = nil
        viewModel = nil
        viewController = nil
    }
}

extension HomeCoordinator: HomeViewModelCoordinatorDelegate {
    func goToCharactersDetail(_ viewModel: HomeViewModel) {
        guard let navigation = navigation else { return }
        charactersDetailCoordinator = CharactersDetailCoordinator(navigation: navigation)
        charactersDetailCoordinator?.resultSelected = viewModel.resultSelected
        charactersDetailCoordinator?.start()
    }
}
