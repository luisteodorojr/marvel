//
//  HomeViewController.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: HomeViewModel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(viewModel: HomeViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel?.delegate = self
        showLoading(isLoading: true)
        viewModel.getListCharacters()
        title = "Marvel"
        initTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setTitleNavigationItem()
    }
    
    private func initTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HomeTableViewCell.self)
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.listResult.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return tableView.dequeueReusableCell(of: HomeTableViewCell.self, for: indexPath) { cell in
            let item = self.viewModel.listResult[indexPath.row]
            cell.titleLabel.text = item.name
            cell.downloadImages(imageURL: self.viewModel.getImgeURL(thumbnail: item.thumbnail))
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.viewModel.listResult.count > 0 {
            let item = self.viewModel.listResult[indexPath.row]
            viewModel.goToCharactersDetail(resultSelected: item)

        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (tableView.contentOffset.y + tableView.frame.size.height) > tableView.contentSize.height && viewModel.isLastPagination {
            showLoading(isLoading: true)
            viewModel.incrementPagination()
            viewModel.getListCharacters()
        }
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func getListCharactersFailure(error: GenericError) {
        alertError(error: error)
    }
    
    func showLoading(_ isLoading: Bool) {
        tableView.reloadData()
        showLoading(isLoading: isLoading)
    }
}
