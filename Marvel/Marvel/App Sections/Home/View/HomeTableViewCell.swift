//
//  HomeTableViewCell.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 24/03/21.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageViewItem: UIImageView!
    
    func downloadImages(imageURL: String) {
        guard let url = URL(string: imageURL) else { return }
        imageViewItem.kf.indicatorType = .activity
        imageViewItem.kf.setImage(with: url) { status in
            if case .failure = status {
                self.imageViewItem.image = UIImage(named: "image_not_available")
            }
        }
    }
    
}
