//
//  HomeModel.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

struct RequestConfig: Codable {
    var limit: String?
    var offset: String?
    var apikey: String?
    var hash: String?
    var ts: String?
    var orderBy: String?
}

struct APIResult: Codable {
    var data: APIData?
}

struct APIData: Codable {
    var count: Int?
    var results: [Results]?
    var total: Int?
}

struct Results: Codable {
    var comics: Comics?
    var name: String?
    var id: Int?
    var resourceURI: String?
    var description: String?
    var thumbnail: Thumbnail?
}

struct Comics: Codable {
    var items: [Items]?
}

struct Items: Codable {
    var name: String?
    var resourceURI: String?
}

struct Thumbnail: Codable {
    var path: String?
    var extensionImage: String?
    
    enum CodingKeys: String, CodingKey {
        case extensionImage = "extension"
        case path
    }
}
