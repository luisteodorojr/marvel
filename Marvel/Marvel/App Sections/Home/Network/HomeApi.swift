//
//  HomeApi.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class HomeApi: Requester {

    func getListCharacters(parameters: RequestConfig,
                           completion: @escaping (Result<APIResult, GenericError>, URLResponse?) -> Void) {
        let request =  EndpointRouter.getListCharacters(parameters: parameters)
        dataTask(using: request, completion: completion)
    }
}
