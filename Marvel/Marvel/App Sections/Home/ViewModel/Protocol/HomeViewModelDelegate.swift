//
//  HomeViewModelDelegate.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation

protocol HomeViewModelDelegate: AnyObject {
    func showLoading(_ isLoading: Bool)
    func getListCharactersFailure(error: GenericError)
}

protocol HomeViewModelCoordinatorDelegate: AnyObject {
    func goToCharactersDetail(_ viewModel: HomeViewModel)
}
