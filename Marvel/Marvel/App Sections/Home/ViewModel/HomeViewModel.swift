//
//  HomeViewModel.swift
//  Marvel
//
//  Created Luis Teodoro Junior on 23/03/21.
//  Copyright © 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit

class HomeViewModel {
    
    weak var delegate: HomeViewModelDelegate?
    weak var coordinatorDelegate: HomeViewModelCoordinatorDelegate?
    
    var limit = 20
    var offset = 0
    var pagination = 1
    var aPIResult: APIResult?
    var listResult: [Results] = [Results]()
    var resultSelected: Results?
    
    var isLastPagination: Bool {
        return aPIResult?.data?.total ?? 0 >= offset
    }
    
    func getListCharacters() {
        HomeApi().getListCharacters(parameters: getRequestConfig(), completion: { [weak self] result, _  in
            switch result {
            case .success(let aPIResult):
                self?.aPIResult = aPIResult
                self?.listResult.append(contentsOf: aPIResult.data?.results ?? [Results]())
            case .failure(let error):
                self?.delegate?.getListCharactersFailure(error: error)
            }
            self?.delegate?.showLoading(false)
        })
        
    }
    
    func getRequestConfig() -> RequestConfig {
        let ts = Date().timeIntervalSinceReferenceDate.description
        let publicAPIKey = MarvelAPIConfig.shared.publicAPIKey
        let hash = Crypto().MD5(data: "\(ts)\(MarvelAPIConfig.shared.privateAPIKey)\(publicAPIKey)")
        
        var requestModel = RequestConfig()
        requestModel.limit = "\(limit)"
        requestModel.offset = "\(offset)"
        requestModel.apikey = publicAPIKey
        requestModel.hash = hash
        requestModel.ts = ts
        requestModel.orderBy = "name"
        return requestModel
    }
    
    func getImageConfig() -> RequestConfig {
        let ts = Date().timeIntervalSinceReferenceDate.description
        let publicAPIKey = MarvelAPIConfig.shared.publicAPIKey
        let hash = Crypto().MD5(data: "\(ts)\(MarvelAPIConfig.shared.privateAPIKey)\(publicAPIKey)")
        
        var requestModel = RequestConfig()
        requestModel.apikey = publicAPIKey
        requestModel.hash = hash
        requestModel.ts = ts
        return requestModel
    }
    
    func getImgeURL(thumbnail: Thumbnail?) -> String {
        return "\(thumbnail?.path ?? "").\(thumbnail?.extensionImage ?? "")"
    }
    
    func incrementPagination() {
        pagination += 1
        offset = pagination * limit
    }
    
    func goToCharactersDetail(resultSelected: Results) {
        self.resultSelected = resultSelected
        coordinatorDelegate?.goToCharactersDetail(self)
    }
    
}
