//
//  UICollectionView+Extension.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 25/03/21.
//

import UIKit

extension UICollectionView {
    func register(_ cell: UICollectionViewCell.Type) {
        let nib = UINib(nibName: cell.identifier, bundle: nil)
        register(nib, forCellWithReuseIdentifier: cell.identifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(of class: T.Type,
                                                      for indexPath: IndexPath,
                                                      configure: @escaping ((T) -> Void) = { _ in }) -> UICollectionViewCell {
        let cell = dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath)
        if let typedCell = cell as? T {
            configure(typedCell)
        }
        return cell
    }
    
    func reloadData(_ completion: ((Bool) -> Void)?) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion: { finish in
            completion?(finish)
        })
    }
}
