//
//  Endpoint.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import Foundation
import Alamofire

enum EndpointRouter: URLRequestConvertible {
    case getListCharacters(parameters: RequestConfig)
    case getListComics(parameters: RequestConfig, id: String)

    static let baseURLString = "https://gateway.marvel.com:443"

    var method: HTTPMethod {
        switch self {
        case .getListCharacters, .getListComics:
            return .get
            
        }
    }

    var path: String {
        switch self {
        case .getListCharacters:
            return "/v1/public/characters"
        case .getListComics(parameters: _, id: let id):
            return "/v1/public/characters/\(id)/comics"
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try EndpointRouter.baseURLString.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        switch self {
        case .getListCharacters(let parameters):
            urlRequest = try URLEncodedFormParameterEncoder.default.encode(parameters, into: urlRequest)
        case .getListComics(parameters: let parameters, id: _):
            urlRequest = try URLEncodedFormParameterEncoder.default.encode(parameters, into: urlRequest)
        }

        return urlRequest
    }
}
