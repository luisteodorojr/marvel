//
//  Requester.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityLogger

class Requester: NSObject {
    
    override init() {
        #if DEBUG
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
        #endif
    }
    
    func dataTask<T: Codable>(
        using request: EndpointRouter,
        completion: ((Result<T, GenericError>, _ response: URLResponse?) -> Void)?) {
        let genericError = GenericError(code: -1, status: "Unknown error")
        
        AF.request(request).responseJSON { response in
            switch response.result {
            case .success:
                let error = self.checkAPIError(response.data)
                if error?.status != "Ok" {
                    completion?(.failure(error!), response.response)
                    return
                }
                if let data = response.data {
                    if let responseDecoded = self.JSONDecode(to: T.self, from: data) {
                        completion?(.success(responseDecoded), response.response)
                        return
                    }
                } else {
                    completion?(.failure(genericError), response.response)
                }
            case .failure:
                completion?(.failure(genericError), response.response)
            }
            
        }
    }
    
    public func checkAPIError(_ data: Data?) -> GenericError? {
        guard let data = data else { return nil }
        guard let responseDecoded = self.JSONDecode(to: GenericError.self, from: data) else {
            return nil
        }
        return responseDecoded
    }
    
    func JSONDecode<T: Codable>(to object: T.Type, from data: Data) -> T? {
        do {
            let object = try JSONDecoder().decode(T.self, from: data) as T
            return object
        } catch let error {
            if object != GenericError.self {
                #if DEBUG
                print("\n❓JSONDecoder -> \(T.self): \(error)\n")
                #endif
            }
            return nil
        }
    }
}
