//
//  Crypto.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 24/03/21.
//

import UIKit
import CryptoKit

class Crypto: NSObject {
    func MD5(data: String) -> String {
        let hash = Insecure.MD5.hash(data: data.data(using: .utf8) ?? Data())
        return hash.map {
            String(format: "%02hhx", $0)
        }.joined()
    }
}
