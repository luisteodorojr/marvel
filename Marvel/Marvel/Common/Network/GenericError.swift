//
//  GenericError.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import Foundation

struct GenericError: Codable, Error {
    public var code: Int?
    public var message: String?
    public var status: String?
}
