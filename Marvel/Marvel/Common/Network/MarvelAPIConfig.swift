//
//  MarvelAPIConfig.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 24/03/21.
//

struct MarvelAPIConfig {
    var privateAPIKey: String
    var publicAPIKey: String

    static var shared: MarvelAPIConfig {
        MarvelAPIConfig(
            privateAPIKey: "8fd3e4c22ae28b7f8f906c18dec924104bafc0a6",
            publicAPIKey: "f0863468747a8695bc06a3191061f195"
        )
    }
}
