//
//  HeaderHandler.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import UIKit

struct HeaderHandler {
    typealias Header = [String: String]

    enum HeaderType {
        case basic
    }
    
    func generate(header: HeaderType) -> [String: String] {
        switch header {
        case .basic:
            return self.getBasicHeader()
        }
    }
    
    private func getBasicHeader() -> Header {
        let dictionary = ["Content-Type": "application/json"]
        return dictionary
    }
}
