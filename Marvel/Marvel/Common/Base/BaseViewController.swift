//
//  BaseViewController.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 24/03/21.
//

import UIKit
import SwiftMessages

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func dismissAnimated() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupDismissLeftBarButtonItem(image: UIImage? = nil) {
        let closeBarButtonItem = UIBarButtonItem(
            image: image != nil ? image : #imageLiteral(resourceName: "btn_back").withRenderingMode(.alwaysOriginal),
            style: .plain,
            target: self,
            action: #selector(self.dismissAnimated)
        )
        navigationItem.leftBarButtonItem = closeBarButtonItem
    }
    
    func setNavigationBarHidden(_ hidden: Bool, animated: Bool) {
        navigationController?.setNavigationBarHidden(hidden, animated: animated)
        navigationController?.isNavigationBarHidden = hidden
    }
    
    func showLoading(isLoading: Bool) {
        DispatchQueue.main.async {
            if isLoading {
                self.view.showHUD()
            } else {
                self.view.hideHUD()
            }
        }
    }
    
    func alertError(error: GenericError) {
        let alertError = MessageView.viewFromNib(layout: .cardView)
        alertError.configureTheme(.error)
        alertError.configureContent(title: "Error", body: error.status ?? "Unknown error")
        alertError.button?.isHidden = true
        alertError.configureDropShadow()
        SwiftMessages.show(view: alertError)

    }
    
    func setTitleNavigationItem() {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "marvel_logo")
        imageView.image = image
        navigationItem.titleView = imageView
    }
}
