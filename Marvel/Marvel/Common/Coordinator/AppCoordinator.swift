//
//  AppCoordinator.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import UIKit

class AppCoordinator {
    
    var window: UIWindow
    var homeCoordinator: HomeCoordinator?
    
    required init(window: UIWindow) {
        self.window = window
        self.window.makeKeyAndVisible()
    }
    
    func start() {
        homeCoordinator = HomeCoordinator(window: self.window)
        homeCoordinator?.start()
    }

}
