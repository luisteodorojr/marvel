//
//  AppDelegate.swift
//  Marvel
//
//  Created by Luis Teodoro Junior on 23/03/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
var window: UIWindow?
var appCoordinator: AppCoordinator!

func application(_ application: UIApplication,
                 didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    window = UIWindow()
    appCoordinator = AppCoordinator(window: window!)
    appCoordinator.start()
    
    return true
}

func applicationWillResignActive(_ application: UIApplication) {
   
}

func applicationDidEnterBackground(_ application: UIApplication) {
  
}

func applicationWillEnterForeground(_ application: UIApplication) {
   
}

func applicationDidBecomeActive(_ application: UIApplication) {
  
}

func applicationWillTerminate(_ application: UIApplication) {
}
}
